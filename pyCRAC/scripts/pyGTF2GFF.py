#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2018"
__version__		= "0.0.2"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "alpha"

##################################################################################
#
#	pyGTF2GFF.py
#
#
#	Copyright (c) Sander Granneman 2018
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import sys
from optparse import *
from pyCRAC.Classes.NGSFormatWriters import NGSFileWriter
from pyCRAC.Classes.NGSFormatReaders import NGSFileReader

def gtf2gff(gtf_file,outfile):
	""" converts gtf files to gff files the way I like it """
	gtfreader = NGSFileReader(gtf_file)		# gtf reader object
	gffwriter = NGSFileWriter(outfile)		# gff writer object
	while gtfreader.readGTFLine():
		names = list()
		if gtfreader.gene_name:
			names.append(gtfreader.gene_name)
		elif gtfreader.gene_id:
			names.append(gtfreader.gene_id)
		if gtfreader.transcript_name:
			names.append(gtfreader.transcript_name)
		elif gtfreader.transcript_id:
			names.append(gtfreader.transcript_id)
		if names:
			names = list(set(names))	# remove any duplicates, for example if the gene_name and transcript_name are identical
		else:
			names = [name]			# if it can't find any attributes, just print the name of the experiment in the output file
		# write to output file:
		names = ",".join(names)
		gffwriter.writeGFF(gtfreader.chromosome,gtfreader.source,gtfreader.feature,gtfreader.start,gtfreader.end-1,gtfreader.score,gtfreader.strand,gtfreader.frame,gene_name=names,gene_id=names)

def main():
	parser = OptionParser(usage="usage: %prog [options] --gtf=gtf_file.gtf", version="%prog 0.0.3")
	files = OptionGroup(parser, "File input options")
	files.add_option("--gtf",dest="gtf_file",metavar="Yourfavoritegtf.gtf",help="type the path to the gtf file that you want to convert. Default is standard input",default=None)
	files.add_option("-o","--outfile",dest="outfile",help="type the name and path of the file you want to write the output to. Default is standard output",default=None)
	parser.add_option_group(files)
	(options, args) = parser.parse_args()
	data = sys.stdin
	out	 = sys.stdout
	
	if options.gtf_file: data = open(options.gtf_file,"r")
	if options.outfile : out = open(options.outfile,"w")
	
	gtf2gff(data,out)
	
if __name__ == "__main__":
	main()